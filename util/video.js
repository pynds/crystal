const { execFile } = require('child_process');
const util = require('util');
const path = require('path');
const ffmpeg = 'ffmpeg.exe';

const execFileAsync = util.promisify(execFile);

async function duration(input) {
	const getVideoDurationCommand = [
		'-i',
		input
	];

	// always report ERROR
	const data = await execFileAsync(ffmpeg, getVideoDurationCommand, { cwd: path.resolve(__dirname, './') })
		.catch((err) => {
			const output = err.stderr.split('\r\n');
			let duration;

			for (let index = 0; index < output.length; index++) {
				if (/^\s+Duration:/ig.test(output[index])) {
					duration = output[index].split(',')[0].substr('  Duration: '.length);
					break;
				}
			}

			return duration;
		});

	return data;
}

function segment(duration) {
	const TIME = duration.substr(0, duration.length - '000'.length).split(':');
	console.log(TIME);
	const [hour, minute, seconds] = [Number(TIME[0]), Number(TIME[1]), Number(TIME[2])];
	const videoTime = Math.floor((hour * 60 * 60) + (minute * 60) + seconds);
	return { segmentTime: Math.floor(videoTime / (100 * 0.3)), seconds: videoTime };
}

module.exports = {
	duration,
	segment
};
