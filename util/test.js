const { duration, segment } = require('./video');
const { exec } = require('child_process');
const chalk = require('chalk');
const path = require('path');
const { scanDir } = require('../src/server/util');

const input = 'D:\\#1影视文件夹\\[Mabors-Sub] One Room S2 [00~04][1080P][HEVC yuv420p10 AAC GB&BIG5]\\[Mabors-Sub] One Room S2 [04][1080P][HEVC yuv420p10 AAC GB&BIG5].mkv';

function promise(cmd) {
	return new Promise((resolve, reject) => {
		exec(cmd, (err) => {
			if (err) return reject(err);
			return resolve();
		});
	});
}

duration(input)
	.then(async (duration) => {
		const { segmentTime, seconds } = segment(duration);

		let i = 0;
		for (let index = 0; index < seconds; index++) {
			if (index !== 0 && index % segmentTime === 0) {
				i += 1;
				await promise(`ffmpeg.exe -y -ss ${index} -i "${input}" -vframes 1 -f image2 -vf scale=-1:160 TEMP/temp_${i}.jpg`);
			}
		}
	})
	.then(() => promise('mogrify.exe -strip -interlace Plane -gaussian-blur 0.05 -quality 75% TEMP/*.jpg'))
	.then(async () => {
		let temp = '';
		const files = await scanDir(path.join(__dirname, './TEMP'));
		for (let i = 0; i < files.length; i++) {
			temp += `TEMP/temp_${i + 1}.jpg `;
		}
		return promise(`convert.exe ${temp} +append temp.jpg`);
	})
	.catch((err) => {
		console.log(chalk.red(err));
	});
