const fs = require('fs');
const path = require('path');

exports.scanDir = (dir) => {
	return new Promise((resolve, reject) => {
		const result = [];

		function walk(dir) {
			const list = fs.readdirSync(dir);

			if (!list.length) return resolve(result);
			list.forEach((file) => {
				const filePath = path.join(dir, file);
				const state = fs.statSync(filePath);

				if (state.isDirectory()) return walk(filePath)
				if (state.isFile()) return result.push(filePath);
			})
		}

		walk(dir);
		return resolve(result);
	})
}
