const Koa = require('koa');
const router = require('./routes');
const bodyparser = require('koa-bodyparser');
const elastic = new (require('elasticsearch')).Client({
	host: [
		{
			host: 'localhost',
			protocol: 'http',
			port: 9200
		}
	],
	log: 'error'
});
const portfinder = require('portfinder');
portfinder.basePort = '3000';

const app = new Koa();

const host = process.env.HOST || '127.0.0.1';

export default function startServer(BrowserWindow) {

	app.use(async (ctx, next) => {
		ctx.elastic = elastic;
		await next();
	})

	app.use(async (ctx, next) => {
		ctx.BrowserWindow = BrowserWindow;
		await next();
	})

	app.use(bodyparser({
		enableTypes: ['json', 'form', 'text']
	}));

	app.use(router.routes());

	portfinder.getPortPromise()
		.then((port) => {
			app.listen(port, host);

			console.log(`Server listening on http://${host}:${port}`);
		})
		.catch((err) => {
			throw err;
		});
}
