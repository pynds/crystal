import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuetify from 'vuetify';
import { remote } from 'electron';

import App from './App';
import router from './router';
import store from './store';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.use(VueI18n);
Vue.use(Vuetify, {
	inconfont: 'md'
});
Vue.config.productionTip = false;

const i18n = new VueI18n({
	locale: remote.app.getLocale() || 'en',
	fallbackLocale: 'en',
	messages: {
		en: require('./locales/en'),
		'zh-CN': require('./locales/zh-CN')
	}
});

/* eslint-disable no-new */
new Vue({
	components: { App },
	router,
	store,
	i18n,
	template: '<App/>',
}).$mount('#app');
