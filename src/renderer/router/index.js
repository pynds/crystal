import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'index',
			component: require('@/pages/index').default,
		},
		{
			path: '/home',
			name: 'home',
			component: require('@/pages/home').default,
		},
		{
			path: '/initial',
			name: 'initial',
			component: require('@/pages/initial').default,
		},
		{
			path: '/mklink',
			name: 'mklink',
			component: require('@/pages/mklink').default,
			props: route => ({ folder: route.query.folder })
		},
		{
			path: '*',
			redirect: '/',
		},
	],
});
